# NCR Cloud Market

##About: 
####TeamName: 
Emojis
####Project title: 
The Solution NCR Needs!
####Running the code:
#####Front end: 
Run in terminal: npm install -g @angular/cli
<br />
Then: npm install
<br />
And: ng serve to run the website

####Project  Summary:

A self-engineered deep learning solution to predict and automate inventory, specifically targeting small retailers. This solves a major technical and timely problem for small business owners who may not have the time and resources to do it themselves.

####Technologies Used:

Angular, Typescript, HTML, CSS, Tensorflow, Keras, Python, Firebase, GKE, Kubernetes, Istio, GCP, Docker, Bitbucket

Link to source code:

https://bitbucket.org/lge_92/cloud-market/src/master/

https://bitbucket.org/lge_92/market-mesh/src/master/
