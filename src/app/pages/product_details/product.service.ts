import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject, Subscriber} from 'rxjs';
import { of , from} from 'rxjs';
 
@Injectable()

export class Product{


  public customerid: string;
  public dates: string[];
  public itemid: string;
  public stock_list: number[] = [];
  public price_list: number[] = [];
  public sold_list: number[] = [];
  public purchased_list: number[] = [];

}