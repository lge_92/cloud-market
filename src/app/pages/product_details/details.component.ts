import { Component, OnInit } from "@angular/core";
import Chart from 'chart.js';
import { Observable, of } from 'rxjs';
import { DataService } from '../tables/data.service';
import {DetailsService} from './details.service';
import {Product} from './product.service';
import { max, min } from 'rxjs/operators';

@Component({
  selector: "app-details",
  templateUrl: "details.component.html"
})
export class DetailsComponent implements OnInit {
  public canvas: any;
  public ctx;
  public datasets: any;
  public data: any;
  public myChartData;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public clicked2: boolean = false;

  columns: string[];
  dataTable: {
      current_inventory: number,
      desired_inventory: any,
      current_price: any,
      predicted_price: any,
      purchase: any

  } = 
  {current_inventory: 3,
    desired_inventory: 1,
    current_price: 1,
    predicted_price: 1,
    purchase: 2
  };
  public product$: Observable<Product> = of();
  public product: Product;
  //purchased_list needs to be under purchase but for "dynamic" data it will be under desired inventory
  constructor(private dataService: DataService, private detailsService: DetailsService) {
    this.product$ = this.detailsService.getDetails();
    this.product$.subscribe(_ => {this.product = _;
            this.dataTable.current_inventory = Math.round(_.stock_list[_.stock_list.length-1]);
            this.dataTable.current_price= _.price_list[_.price_list.length-1];
            this.dataTable.desired_inventory = Math.round(_.purchased_list[_.purchased_list.length-1]);
            this.dataTable.predicted_price = _.sold_list[_.sold_list.length-1];
            this.dataTable.purchase = Math.round(_.purchased_list[_.purchased_list.length-1]- _.stock_list[_.stock_list.length-1])});
  }

  ngOnInit() {
    this.columns = this.dataService.getColumnsInDetails();
    () => console.log(this.dataTable.current_inventory)
    var gradientChartOptionsConfigurationWithTooltipBlue: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#2380f7"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#2380f7"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipPurple: any = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'right'
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipRed: any = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'right'
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 30,
            suggestedMax:120 ,
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipOrange: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 50,
            suggestedMax: 110,
            padding: 20,
            fontColor: "#ff8a76"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#ff8a76"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipGreen: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 0,
            suggestedMax: 125,
            padding: 10,
            fontColor: "#9e9e9e"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }]
      }
    };


    var gradientBarChartConfiguration: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: "Happiness"
          },
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 1,
            suggestedMax: 20,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }],

        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: "GDP (PPP)"
          },
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }]
      }
    };

    this.canvas = document.getElementById("chartLineRed");
    this.ctx = this.canvas.getContext("2d");

    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(84, 185, 72,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(84, 185, 72,0.0)');
    gradientStroke.addColorStop(0, 'rgba(84, 185, 72,0)'); //red colors

    var data = {
      labels: this.product.dates,
      datasets: [{
        label: "Actual",
        fill: true,
        backgroundColor: gradientStroke,
        borderColor: '#54B948',
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: '#54B948',
        pointBorderColor: 'rgba(84, 185, 72,0)',
        pointHoverBackgroundColor: '#54B948',
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 0,
        data: this.product.price_list,
      },
        {
          label: "Predicted",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: '#C0C0C0',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#C0C0C0',
          pointBorderColor: 'rgba(192,192,192,0)',
          pointHoverBackgroundColor: '#C0C0C0',
          pointBorderWidth: 20,
          pointHoverRadius: 2,
          pointHoverBorderWidth: 15,
          pointRadius: 0,
          data: this.product.sold_list,
        }]

    };

    var myChart = new Chart(this.ctx, {
      type: 'line',
      data: data,
      options: gradientChartOptionsConfigurationWithTooltipRed
    });

    var chart_labels = this.product.dates;
    this.datasets = [
      this.product.stock_list,
      this.product.purchased_list,
      this.product.sold_list
    ];
    this.data = this.datasets[0];



    this.canvas = document.getElementById("chartBig1");
    this.ctx = this.canvas.getContext("2d");

    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(84, 185, 72, 0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(84, 185, 72, 0.0)');
    gradientStroke.addColorStop(0, 'rgba(84, 185, 72, 0)'); //red colors

    var config = {
      type: 'line',
      data: {
        labels: chart_labels,
        datasets: [{
          label: "My First dataset",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: '#54B948',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#54B948',
          pointBorderColor: 'rgba(84, 185, 72,0)',
          pointHoverBackgroundColor: '#54B948',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: this.data,
        }]
      },
      options: gradientChartOptionsConfigurationWithTooltipGreen
    };
    this.myChartData = new Chart(this.ctx, config);


    this.canvas = document.getElementById("CountryChart");
    this.ctx  = this.canvas.getContext("2d");
    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(84, 185, 72,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(84, 185, 72,0.0)');
    gradientStroke.addColorStop(0, 'rgba(84, 185, 72,0)'); //blue colors


    var myChart = new Chart(this.ctx, {
      type: 'bubble',
      responsive: true,
      legend: {
        display: false
      },
      data: {
        labels: "Africa",
        datasets: [
          {
            label: ["Milk"],
            backgroundColor: "rgba(255,221,50,0.2)",
            borderColor: "rgba(255,221,50,1)",
            data: [{
              x: 20,
              y: 4.5,
              r: 10
            }]
          }, {
            label: ["Bread"],
            backgroundColor: "rgba(60,186,159,0.2)",
            borderColor: "rgba(60,186,159,1)",
            data: [{
              x: 5,
              y: 7,
              r: 16
            }]
          }, {
            label: ["Cheese"],
            backgroundColor: "rgba(0,0,0,0.2)",
            borderColor: "#000",
            data: [{
              x: 10,
              y: 9,
              r: 16
            }]
          }, {
            label: ["Pepperonis"],
            backgroundColor: "rgba(193,46,12,0.2)",
            borderColor: "rgba(193,46,12,1)",
            data: [{
              x: 15,
              y: 12.5,
              r: 20
            }]
          }
        ]
      },
      options: gradientBarChartConfiguration
    });

  }
  public updateOptions() {
    this.myChartData.data.datasets[0].data = this.data;
    this.myChartData.update();
  }
}
