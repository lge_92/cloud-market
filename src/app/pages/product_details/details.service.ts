import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, Subject, Subscriber} from 'rxjs';
import { of } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Product} from './product.service';
import {map} from 'rxjs/operators'
 
@Injectable()

export class DetailsService
{
    private productSource:BehaviorSubject<Product>= new BehaviorSubject(new Product());
    private product: Product;
    private productUrl: string = 'http://34.68.247.61/customer/company-1/item/';
    public Table:    {
        current_inventory: any,
        desired_inventory: any,
        current_price: any,
        predicted_price: any,
        purchase: any

    };
    constructor(private http: HttpClient)
    {
        this.productSource.subscribe(_ => this.product = _);
    }

    createProduct(name: string)
    {
        this.http.get(this.productUrl + name.toLowerCase()).subscribe( (data: any) => {
            data.itemid = data.itemid.charAt(0).toUpperCase() + data.itemid.slice(1);
            this.productSource.next(data);
            console.log(this.product);
        })
    }

    getDetails(): Observable<Product>
    {
        return this.productSource;
    }

}
