import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import {delay} from 'rxjs/operators';

@Injectable()
export class DataService {
  data: any[] = [
    {
      product: 'Apple',
      current_inventory: 2,
      desired_inventory: 7,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 5
    },
    {
      product: 'Beer',
      current_inventory: 3,
      desired_inventory: 11,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 8
    },
    {
      product: 'Bottle Water',
      current_inventory: 5,
      desired_inventory: 13,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 11
    },
    {
      product: 'Bread',
      current_inventory: 8,
      desired_inventory: 17,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 14
    },
    {
      product: 'Cheetos',
      current_inventory: 2,
      desired_inventory: 7,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 5
    },
    {
      product: 'Coke',
      current_inventory: 2,
      desired_inventory: 7,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 5
    },
    {
      product: 'Milk',
      current_inventory: 2,
      desired_inventory: 7,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 5
    },
    {
      product: 'String Cheese',
      current_inventory: 2,
      desired_inventory: 7,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 5
    },
  ];

  data1: any[] = 
    [{
      current_inventory: 2,
      desired_inventory: 7,
      current_price: 2.89,
      predicted_price: 3.12,
      purchase: 5
    }];
  constructor() { }

  getColumnsInAnalytics(): string[] {
    return ["product", "current_inventory", "desired_inventory", "purchase"];
  }

  getColumns(): string[] {
    return ["select", "product", "current_inventory", "desired_inventory", "purchase"];
  }


  getColumnsInDetails(): string[] {
    return ["current_inventory", "desired_inventory", "current_price", "predicted_price", "purchase"];
  }

  getData(): Observable<any[]> {
    return of(this.data).pipe(delay(100));
  }

  getDataDetails(): Observable<any>
  {
    console.log(this.data1);
    return of(this.data1);
  }
}
