import {Component, Input, OnInit} from '@angular/core';
import { DataService } from './data.service';
import { Observable, of} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: "app-tables",
  templateUrl: "tables.component.html"
})
export class TablesComponent implements OnInit {
  private productTable: any[];
  private getProductsUrl: string = 'http://34.68.247.61/customer/company-1/item';
  columns: string[];
  data: Observable<any[]>;

  constructor(private http: HttpClient, private dataService: DataService, private toastr: ToastrService) {}

  decrease(item: any) {
    if (item.purchase > 0) {
      item.purchase--;
    }
  }

  increase(item: any) {
    item.purchase++;
  }

  ngOnInit() {
    this.columns = this.dataService.getColumns();
    // this.data = this.dataService.getData();
    this.getProducts().subscribe((data: any) => {
      var items: any[] = [];
      for (var i = 0; i < data.item_list.length; i++) {
        items.push({
          product: data.item_list[i].charAt(0).toUpperCase() + data.item_list[i].slice(1),
          current_inventory: Math.round(data.current[i]),
          desired_inventory: Math.round(data.predicted[i]),
          purchase: Math.round(data.purchased[i])
        });
      }
      // data.item_list.forEach((item) => {
      //   items.push({
      //     product: item.charAt(0).toUpperCase() + item.slice(1),
      //     current_inventory: 8,
      //     desired_inventory: 17,
      //     purchase: 14
      //   })
      // })
      this.data = of(items);
    })
  }

  getProducts() {
    return this.http.get(this.getProductsUrl);
  }

  orderPlacedNotification(from, align) {
    this.toastr.info('<span class="tim-icons icon-bell-55" [data-notify]="icon"></span><b>Order Placed!</b> - Thanks for using NCR Cloud Market.', '', {
      disableTimeOut: true,
      closeButton: true,
      enableHtml: true,
      toastClass: "alert alert-info alert-with-icon",
      positionClass: 'toast-' + from + '-' +  align
    });
  }

  saveNotification(from, align) {
    this.toastr.info('<span class="tim-icons icon-bell-55" [data-notify]="icon"></span><b>Order Saved!</b> - Thanks for using NCR Cloud Market.', '', {
      disableTimeOut: true,
      closeButton: true,
      enableHtml: true,
      toastClass: "alert alert-info alert-with-icon",
      positionClass: 'toast-' + from + '-' + align
    });
  }

  deleteNotification(from, align) {
    this.toastr.info('<span class="tim-icons icon-bell-55" [data-notify]="icon"></span><b>Order Deleted!</b> - Thanks for using NCR Cloud Market.', '', {
      disableTimeOut: true,
      closeButton: true,
      enableHtml: true,
      toastClass: "alert alert-info alert-with-icon",
      positionClass: 'toast-' + from + '-' + align
    });
  }
}
