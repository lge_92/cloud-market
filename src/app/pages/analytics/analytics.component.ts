import { Component, OnInit } from "@angular/core";
import Chart from 'chart.js';
import { Observable } from 'rxjs';
import {DataService} from '../tables/data.service';

@Component({
  selector: "app-dashboard",
  templateUrl: "analytics.component.html"
})
export class AnalyticsComponent implements OnInit {
  public canvas: any;
  public ctx;
  public datasets: any;
  public data: any;
  public myChartData;
  public clicked: boolean = true;
  public clicked1: boolean = false;
  public clicked2: boolean = false;

  columns: string[];
  dataTable: Observable<any[]>;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.columns = this.dataService.getColumnsInAnalytics();
    this.dataTable = this.dataService.getData();

    var gradientChartOptionsConfigurationWithTooltipBlue: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#2380f7"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#2380f7"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipPurple: any = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'right'
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipRed: any = {
      maintainAspectRatio: false,
      legend: {
        display: true,
        position: 'right'
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 60,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9a9a9a"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipOrange: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 50,
            suggestedMax: 110,
            padding: 20,
            fontColor: "#ff8a76"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#ff8a76"
          }
        }]
      }
    };

    var gradientChartOptionsConfigurationWithTooltipGreen: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },

      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.0)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 50,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }],

        xAxes: [{
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }]
      }
    };


    var gradientBarChartConfiguration: any = {
      maintainAspectRatio: false,
      legend: {
        display: false
      },
      tooltips: {
        backgroundColor: '#f5f5f5',
        titleFontColor: '#333',
        bodyFontColor: '#666',
        bodySpacing: 4,
        xPadding: 12,
        mode: "nearest",
        intersect: 0,
        position: "nearest"
      },
      responsive: true,
      scales: {
        yAxes: [{
          scaleLabel: {
            display: true,
            labelString: "Happiness"
          },
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            suggestedMin: 1,
            suggestedMax: 20,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }],

        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: "GDP (PPP)"
          },
          gridLines: {
            drawBorder: false,
            color: 'rgba(84, 185, 72,0.1)',
            zeroLineColor: "transparent",
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }]
      }
    };

    this.canvas = document.getElementById("chartLineRed");
    this.ctx = this.canvas.getContext("2d");

    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(84, 185, 72,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(84, 185, 72,0.0)');
    gradientStroke.addColorStop(0, 'rgba(84, 185, 72,0)'); //red colors

    var data = {
      labels: Array.from(Array(200).keys()),
      datasets: [{
        label: "Actual",
        fill: true,
        backgroundColor: gradientStroke,
        borderColor: '#54B948',
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: '#54B948',
        pointBorderColor: 'rgba(84, 185, 72,0)',
        pointHoverBackgroundColor: '#54B948',
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 0,
        data: [45.93732833862305, 47.85353469848633, 49.85600280761719, 51.95124816894531, 54.05107879638672, 56.282325744628906, 58.64744567871094, 60.90922546386719, 62.956546783447266, 65.2500991821289, 68.38677215576172, 71.17573547363281, 73.7468490600586, 76.14727783203125, 78.41502380371094, 80.56761932373047, 82.4747543334961, 84.20256805419922, 85.84423828125, 87.28787994384766, 88.6211166381836, 89.83780670166016, 90.87468719482422, 91.83184051513672, 92.66015625, 93.36027526855469, 93.94956970214844, 94.43063354492188, 94.81967163085938, 95.08826446533203, 95.2159652709961, 95.22937774658203, 95.1451644897461, 94.98426055908203, 94.6244888305664, 94.094970703125, 93.39994812011719, 92.57688903808594, 91.53497314453125, 90.32904052734375, 88.82894134521484, 87.1364517211914, 85.10669708251953, 82.88494873046875, 80.66045379638672, 78.44573974609375, 75.99400329589844, 73.39617156982422, 70.64815521240234, 67.80091857910156, 64.8093490600586, 61.81875228881836, 58.786495208740234, 55.92000198364258, 53.03300476074219, 50.2800407409668, 47.70983123779297, 45.316612243652344, 43.075252532958984, 41.128875732421875, 39.42195129394531, 37.90555191040039, 36.62079620361328, 35.56809997558594, 34.68937683105469, 34.01197814941406, 33.45136260986328, 33.032371520996094, 32.876102447509766, 32.83185958862305, 32.83414840698242, 32.88209915161133, 32.978641510009766, 33.12430191040039, 33.316253662109375, 33.556819915771484, 33.871620178222656, 34.268577575683594, 34.737464904785156, 35.31452560424805, 36.003509521484375, 36.808231353759766, 37.76322555541992, 38.84659957885742, 40.09967041015625, 41.51335144042969, 43.083709716796875, 44.79551696777344, 46.640411376953125, 48.59012222290039, 50.640380859375, 52.75775146484375, 54.86691665649414, 57.20858383178711, 59.5308952331543, 61.66562271118164, 63.55510330200195, 66.25373840332031, 69.195556640625, 71.80497741699219, 74.30802154541016, 76.70537567138672, 78.95378112792969, 81.0496597290039, 82.94140625, 84.69110107421875, 86.30028533935547, 87.75941467285156, 89.02728271484375, 90.17701721191406, 91.21165466308594, 92.12399291992188, 92.89483642578125, 93.53948211669922, 94.0806655883789, 94.52667236328125, 94.86475372314453, 95.11773681640625, 95.20850372314453, 95.22702026367188, 95.158203125, 94.95220947265625, 94.526611328125, 93.98790740966797, 93.260986328125, 92.38050842285156, 91.32447052001953, 90.03115844726562, 88.56722259521484, 86.8827896118164, 84.82818603515625, 82.57848358154297, 80.38922119140625, 78.14854431152344, 75.67707061767578, 73.0831069946289, 70.34606170654297, 67.50527954101562, 64.52617645263672, 61.53682327270508, 58.664634704589844, 55.7609748840332, 52.978851318359375, 50.143733978271484, 47.621551513671875, 45.208343505859375, 42.97059631347656, 41.02973556518555, 39.295387268066406, 37.78478240966797, 36.536155700683594, 35.491329193115234, 34.63978576660156, 33.96571731567383, 33.42467498779297, 33.014251708984375, 32.87080001831055, 32.83803939819336, 32.83979797363281, 32.887325286865234, 32.97840118408203, 33.117000579833984, 33.30760192871094, 33.55664825439453, 33.87086868286133, 34.263980865478516, 34.7425537109375, 35.3173713684082, 36.0145149230957, 36.82511520385742, 37.77142333984375, 38.87959671020508, 40.152618408203125, 41.58565139770508, 43.18821716308594, 44.92332077026367, 46.804134368896484, 48.773094177246094, 50.86320114135742, 53.000911712646484, 55.11949157714844, 57.498626708984375, 59.8137321472168, 61.988162994384766, 63.97078323364258, 67.02252960205078, 69.8936767578125, 72.55158233642578, 75.05757141113281, 77.42867279052734, 79.58924865722656, 81.6055679321289, 83.4529037475586, 85.11309051513672, 86.68010711669922, 88.10270690917969, 89.32688903808594, 90.40758514404297, 91.36139678955078, 92.23062896728516],
      },
    {
      label: "Predicted",
      fill: true,
      backgroundColor: gradientStroke,
      borderColor: '#C0C0C0',
      borderWidth: 2,
      borderDash: [],
      borderDashOffset: 0.0,
      pointBackgroundColor: '#C0C0C0',
      pointBorderColor: 'rgba(192,192,192,0)',
      pointHoverBackgroundColor: '#C0C0C0',
      pointBorderWidth: 20,
      pointHoverRadius: 2,
      pointHoverBorderWidth: 15,
      pointRadius: 0,
      data:[42.34103821107004, 44.008892746561855, 48.48487896120869, 49.1375451330564, 54.42988303324567, 56.09181594635358, 58.344678585219285, 60.326303456963416, 63.7111233607779, 66.02105249977518, 68.82864762158506, 69.19686029536162, 70.73285507711913, 74.09618306247812, 78.43044548308802, 76.55911676754766, 78.94324077875262, 84.80146053441258, 83.48873240717671, 87.0019697547113, 90.242847301967, 88.33483763045949, 93.25571513143612, 94.68165223084904, 94.82785572227749, 96.83466983653148, 96.8447804676525, 98.91638937622619, 96.97515247525581, 96.96854332446534, 96.71554318071452, 96.3421547453892, 99.1448070656671, 99.7317913789459, 99.15146847824471, 96.5001587218281, 96.66542270000022, 93.05545483429763, 94.47927837304209, 91.01253950212528, 92.20490081777105, 87.68553656576523, 87.07114050986127, 85.63239276940547, 84.88589055983267, 80.14181526492169, 78.1132845783915, 78.77706764030448, 75.94293000261598, 71.48055100876394, 70.05249044563104, 66.81857641926712, 66.7971824304526, 63.36944376754126, 60.53335275669559, 58.62083133899047, 57.88535757376257, 52.586760817372706, 52.64485151399894, 51.32036123111473, 46.62339501024309, 45.123243929755425, 42.76511233250473, 40.94610696801186, 40.13698508467794, 36.6974665636519, 35.656832984576184, 33.67382031534796, 32.21378586646692, 33.02603244224353, 33.31828173820098, 33.56334665878155, 32.9432783867709, 30.765231554978406, 29.07771222301347, 29.78423629596864, 32.10173760702392, 30.13587321732608, 31.631057011057663, 33.4852246991824, 32.921075424634466, 34.100129926863765, 32.91153747628646, 37.49020022393631, 37.12235527208951, 38.810271680471146, 38.83326410762181, 41.62359641673942, 42.598172756268724, 46.12121711869786, 49.42310577128225, 50.05615550744804, 55.162885440544045, 55.70356859356416, 57.237838670944264, 61.93755758853027, 61.91903364161562, 66.700303241551, 65.53290869692687, 71.84869811641529, 73.88897423454551, 75.83525130214423, 78.16354167529826, 78.56897177466163, 82.29636272973933, 84.70751911402056, 86.30004533710145, 85.35872039895017, 88.7644538772461, 91.09017875573375, 93.14055451276491, 92.55296937384544, 93.91698367614633, 95.68462532492985, 97.53303763007185, 97.14117970519195, 99.53130859391749, 97.59841262650401, 100.21301710510305, 100.22221028537467, 98.60774470849967, 95.82381245719391, 99.22508992526403, 95.28579676021651, 95.8278900725721, 94.15369170820426, 91.6253990816265, 94.23368790521033, 92.6817280754005, 87.96031494654495, 87.09527109596577, 85.79657066659874, 84.17832497154808, 80.06437420724394, 78.38326749223313, 79.03141523345512, 75.5478587885201, 71.28696979001512, 69.3012564768114, 69.50215253568169, 65.68309310415188, 64.632839130241, 58.759162694844434, 59.886138122137666, 56.325525059553804, 52.64642217263135, 52.76979623522151, 48.16339511749992, 46.0233722132035, 45.07226563049131, 41.787755479076665, 42.28180842450442, 39.008223498215315, 39.17832596705884, 35.13345469201592, 35.64210056806174, 35.47365226700336, 31.11743569680983, 31.149783278474473, 31.11620194217996, 30.721897723120925, 30.529403494489877, 30.945501320335325, 29.191968393321865, 32.78034900991586, 32.48872920640559, 32.29018123471553, 33.48744238988622, 31.81075787138822, 33.34397981225175, 37.44357087154073, 37.154603055881765, 37.54537807109503, 40.48948572240252, 39.489326175463304, 43.07679040673282, 43.13234467079282, 48.97960573508414, 50.47219561583296, 50.82916177341866, 55.32231957071404, 56.39255707192847, 60.812051100506025, 61.804611048051555, 65.2689742073932, 66.25575444236871, 68.13102096078427, 71.37100693667006, 73.70561769743877, 73.852693985284, 77.45152925271526, 79.68731032797092, 80.78471152390773, 86.13753158100526, 87.474338668809, 85.95517992634527, 87.41724377555217, 89.0993587847593, 94.10475259885395, 93.57566606867394],
    }]
      
    };

    var myChart = new Chart(this.ctx, {
      type: 'line',
      data: data,
      options: gradientChartOptionsConfigurationWithTooltipRed
    });



    var chart_labels = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    this.datasets = [
      [100, 70, 90, 70, 85, 60, 75, 60, 90, 80, 110, 100],
      [80, 120, 105, 110, 95, 105, 90, 100, 80, 95, 70, 120],
      [60, 80, 65, 130, 80, 105, 90, 130, 70, 115, 60, 130]
    ];
    this.data = this.datasets[0];



    this.canvas = document.getElementById("chartBig1");
    this.ctx = this.canvas.getContext("2d");

    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(84, 185, 72, 0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(84, 185, 72, 0.0)');
    gradientStroke.addColorStop(0, 'rgba(84, 185, 72, 0)'); //red colors

    var config = {
      type: 'line',
      data: {
        labels: chart_labels,
        datasets: [{
          label: "My First dataset",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: '#54B948',
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: '#54B948',
          pointBorderColor: 'rgba(84, 185, 72,0)',
          pointHoverBackgroundColor: '#54B948',
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: this.data,
        }]
      },
      options: gradientChartOptionsConfigurationWithTooltipGreen
    };
    this.myChartData = new Chart(this.ctx, config);


    this.canvas = document.getElementById("CountryChart");
    this.ctx  = this.canvas.getContext("2d");
    var gradientStroke = this.ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, 'rgba(84, 185, 72,0.2)');
    gradientStroke.addColorStop(0.4, 'rgba(84, 185, 72,0.0)');
    gradientStroke.addColorStop(0, 'rgba(84, 185, 72,0)'); //blue colors


    var myChart = new Chart(this.ctx, {
      type: 'bubble',
      responsive: true,
      legend: {
        display: false
      },
      data: {
        labels: "Africa",
        datasets: [
          {
            label: ["Milk"],
            backgroundColor: "rgba(255,221,50,0.2)",
            borderColor: "rgba(255,221,50,1)",
            data: [{
              x: 20,
              y: 4.5,
              r: 10
            }]
          }, {
            label: ["Bread"],
            backgroundColor: "rgba(60,186,159,0.2)",
            borderColor: "rgba(60,186,159,1)",
            data: [{
              x: 5,
              y: 7,
              r: 16
            }]
          }, {
            label: ["Cheese"],
            backgroundColor: "rgba(0,0,0,0.2)",
            borderColor: "#000",
            data: [{
              x: 10,
              y: 9,
              r: 16
            }]
          }, {
            label: ["Pepperonis"],
            backgroundColor: "rgba(193,46,12,0.2)",
            borderColor: "rgba(193,46,12,1)",
            data: [{
              x: 15,
              y: 12.5,
              r: 20
            }]
          }
        ]
      },
      options: gradientBarChartConfiguration
    });

  }
  public updateOptions() {
    this.myChartData.data.datasets[0].data = this.data;
    this.myChartData.update();
  }
}
