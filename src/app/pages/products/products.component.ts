import { Component, OnInit } from "@angular/core";
import {HttpClient} from '@angular/common/http';
import {DetailsService} from '../product_details/details.service'

@Component({
  selector: "app-icons",
  templateUrl: "products.component.html"
})
export class ProductsComponent implements OnInit {
  private products: string[];
  private getProductsUrl: string = 'http://34.68.247.61/customer/company-1/item';
  constructor(private http: HttpClient, private detailsService: DetailsService) {
    this.http = http;
  }

  ngOnInit() {
    this.getProducts().subscribe((data: any) => {
      this.products = [];
      data.item_list.forEach((d) => {
        this.products.push(d.charAt(0).toUpperCase() + d.slice(1));
      })
      console.log(data);
    })
  }

  getProducts() {
    return this.http.get(this.getProductsUrl);
  }

  getProduct( name: string)
  {
    this.detailsService.createProduct(name);
  }

}
