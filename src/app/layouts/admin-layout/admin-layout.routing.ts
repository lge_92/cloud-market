import { Routes } from "@angular/router";

import { AnalyticsComponent } from "../../pages/analytics/analytics.component";
import { ProductsComponent } from "../../pages/products/products.component";
import { MapComponent } from "../../pages/map/map.component";
import { NotificationsComponent } from "../../pages/notifications/notifications.component";
import { UserComponent } from "../../pages/user/user.component";
import { TablesComponent } from "../../pages/tables/tables.component";
import { TypographyComponent } from "../../pages/typography/typography.component";
import {DetailsComponent} from '../../pages/product_details/details.component';
// import { RtlComponent } from "../../pages/rtl/rtl.component";

export const AdminLayoutRoutes: Routes = [
  { path: "analytics", component: AnalyticsComponent },
  { path: "product_details", component: DetailsComponent },
  { path: "products", component: ProductsComponent },
  { path: "maps", component: MapComponent },
  { path: "notifications", component: NotificationsComponent },
  { path: "user", component: UserComponent },
  { path: "tables", component: TablesComponent },
  { path: "typography", component: TypographyComponent },
  // { path: "rtl", component: RtlComponent }
];
